package com.example.fernandoambrosio.sesion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class StudentActivity extends AppCompatActivity {

    private Spinner spn1;
    private RadioButton rb1, rb2, rb3;
    private EditText et5;
    private CheckBox ch1, ch2, ch3;
    int buena=0 ,regular=0,mala=0;
    int subtotal;
    String total;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        rb1 = (RadioButton) findViewById(R.id.rb1);
        rb2 = (RadioButton) findViewById(R.id.rb2);
        rb3 = (RadioButton) findViewById(R.id.rb3);
        et5 = (EditText) findViewById(R.id.et5);
        ch1 = (CheckBox) findViewById(R.id.ch1);
        ch2 = (CheckBox) findViewById(R.id.ch2);
        ch3 = (CheckBox) findViewById(R.id.ch3);


        spn1 = (Spinner) findViewById(R.id.spn1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.opciones, android.R.layout.simple_spinner_dropdown_item);
        spn1.setAdapter(adapter);

    }

    public void enviar(View view){
        if (rb1.isChecked()== true){
            subtotal=buena+1;
            total= String.valueOf(subtotal);
            //et5.setText(total);
        }
        else if (rb2.isChecked()== true){
            subtotal=regular+1;
            total= String.valueOf(subtotal);
            //et5.setText(total);
        }
        else if (rb3.isChecked()==true){
            subtotal=mala+1;
            total= String.valueOf(subtotal);
            //et5.setText(total);
        }

        et5.setText("");
        rb1.setText("");
        rb2.setText("");
        rb3.setText("");
        ch1.setText("");
        ch2.setText("");
        ch3.setText("");

        Toast notification = Toast.makeText(this, "Datos enviados Correctamene", Toast.LENGTH_SHORT);
        notification.show();

    }

    public void salir(View view){
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);

    }
}
